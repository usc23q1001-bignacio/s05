# [SECTION] Python Class Review

class SampleClass():
	def __init__(self, year, age):
		self.year = year
		self._age = age

	def show_year(self):
		print(f"The year is: {self.year}")

	def get_age(self):
		print(f"{self._age}")

	def set_age(self, age):
		self._age = age

myObj = SampleClass(2020, 20)

# print(myObj.year)
# myObj.show_year()

# myObj.get_age()
# myObj.set_age(10)
# myObj.get_age()

class Person():
	def __init__(self, name = "Bligh", age = 22):
		self.name = name
		self._age = age

	def get_age(self):
		print(f"{self._age}")

	def set_age(self, age):
		self._age = _age

	def get_name(self):
		print(self.name)

	def set_name(self, name):
		self.name = name

class Employee(Person):
	def __init__(self, employeeId):
		super().__init__()
		self._employeeId = employeeId

	def get_details(self):
		print(f"{self._employeeId} belongs to {self.name}")

emp1 = Employee("Emp-001")
# emp1.get_details()
# emp1.set_name("Liam")
# emp1.set_age(23)
# emp1.get_details()
# emp1.get_age()

class Student(Person):
	def __init__(self, student_no, course, year_level, name = "Joe", age = 24):
		super().__init__(name, age)
		self._student_no = student_no
		self._course = course
		self._year_level = year_level

	def get_student_no(self):
		print(f"Student No is {self.student_no}")

	def set_student_no(self, student_no):
		self.student_no = student_no

	def get_course(self):
		print(f"Student course is {self.course}")

	def set_course(self):
		self.course = course

	def get_year_level(self):
		print(f"Student year level is {self._year_level}")

	def set_year_level(self):
		self.year_level = year_level

	def get_details(self):
		print(f"{self.name} is currently in year {self._year_level} taking up {self._course}")

# student1 = Student(1, "BSCS", 4, "Gekko", 28)
# student1.get_details()
# student1.get_age()

# [SECTION] Polymorphism
# The method inherited from the parent class is not always fit for the child class. Re-implementing/overriding of method can be done in the child class

# There are different methods to use polymorphism in Python

# Function and objects
# A function can be created that can take any object, allowing polymorphism.

class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print("Admin User")

class Customer():
	def is_admin(self):
		print(False)

	def user_type(self):
		print("Customer User")

# Define a test function that will take an object called obj.

def test_function(obj):
	obj.is_admin()
	obj.user_type()

user_admin = Admin()
user_customer = Customer()

# Pass the created instance to test function
# test_function(user_admin)
# test_function(user_customer)

# Polymorphism with Class methods

class TeamLead():
	def occupation(self):
		print("Team Lead")

	def hasAuth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print("Team Member")

	def hasAuth(self):
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

# for person in (tl1, tm1):
# 	person.occupation()

# Polymorphism with Inheritance
# Polymorphism in python defines methods in child class that have the same name as methods in the parent class
# "Method Overriding"

class Zuitt():
	def tracks(self):
		print("We are currently offering 3 tracks(developer career, pi-shape career, and short courses)")

	def num_of_hours(self):
		print("Learn web development in 360 hours!")

class DeveloperCareer(Zuitt):
	# Override the parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn the basics of web development in 240 hours!")

class PiShapedCareer(Zuitt):
	# Override the parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn skills for no-code app development in 140 hours!")

class ShortCourses(Zuitt):
	# Override the parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn advanced topics in web development in 20 hours!")

# course1 = DeveloperCareer()
# course2 = PiShapedCareer()
# course3 = ShortCourses()

# course1.num_of_hours()
# course2.num_of_hours()
# course3.num_of_hours()

# [SECTION] Abstraction
# An abstract class can be considered as a blueprint for other class.

# Abstract base classes (abc)
# The import tells the program to get the abc module of python to be used
from abc import ABC, abstractclassmethod

# This class Polygon inherites the abstract class module
class Polygon(ABC):
	# Created an abstract methods called print_number_of_sides that needs to be implemented by classes that will inherite Polygon class
	@abstractclassmethod
	def print_number_of_sides(self):
		# This denotes that the method doesn't do anything.
		pass

class Triangle(Polygon):
	def __init__(self):
		super().__init__()

	# Since the Triangle class inherited the Polygon class it must now implement the abstract method.
	def print_number_of_sides(self):
		print(f"This polygon has 3 sides")

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print(f"This polygon has 5 sides")

shape1 = Triangle()
shape2 = Pentagon()

shape1.print_number_of_sides()
shape2.print_number_of_sides()
